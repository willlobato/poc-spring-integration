package net.willlobato.poc.spring.integration.process.example01;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.jms.Jms;

import javax.jms.ConnectionFactory;

@Configuration
public class Example01Configuration {

    public static final String QUEUE_DIRECT_OUT = "example01-queue-direct-out";

    /*
    TODO: EXAMPLE GENERIC MODEL WITH DefaultMessageListenerContainer
    @Bean("example01ResponderDMLC")
    public DefaultMessageListenerContainer example01Responder(ConnectionFactory jmsConnectionFactory) {
        DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
        container.setConnectionFactory(jmsConnectionFactory);
        container.setDestinationName(QUEUE_DIRECT_OUT);
        MessageListenerAdapter adapter = new MessageListenerAdapter(new Object() {
            @SuppressWarnings("unused")
            public String handleMessage(String in) {
                return in.toUpperCase();
            }
        });
        container.setMessageListener(adapter);
        return container;
    }
    */

    @Bean("example01InboundGateway")
    public IntegrationFlow example01InboundGateway(ConnectionFactory jmsConnectionFactory) {
        return IntegrationFlows
                .from(Jms.inboundGateway(jmsConnectionFactory)
                        .destination(QUEUE_DIRECT_OUT))
//                .transform((String s) -> s.toUpperCase())
                .<String>handle((payload, headers) -> payload.toUpperCase())
                .get();
    }

    /*
    public GenericHandler messageHandler() {
        GenericHandler<String> stringGenericHandler = (payload, headers) -> payload.toUpperCase();
        return stringGenericHandler;
    }
    */
}
