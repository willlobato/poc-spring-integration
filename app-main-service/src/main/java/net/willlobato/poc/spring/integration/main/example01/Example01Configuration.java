package net.willlobato.poc.spring.integration.main.example01;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.messaging.MessageChannel;

/**
 *
 */
@Configuration
public class Example01Configuration {

    public static final String QUEUE_DIRECT_OUT = "example01-queue-direct-out";
    public static final String QUEUE_DIRECT_IN = "example01-queue-direct-in";
    public static final String DIRECT_CHANNEL = "example01-direct-channel";

    @Bean(name = DIRECT_CHANNEL)
    public MessageChannel directChannel() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow example01OutboundGateway(ActiveMQConnectionFactory jmsConnectionFactory) {
        return IntegrationFlows.from(DIRECT_CHANNEL)
                .handle(Jms.outboundGateway(jmsConnectionFactory)
                        .requestDestination(QUEUE_DIRECT_OUT)
                        .replyDestination(QUEUE_DIRECT_IN)
                        .receiveTimeout(10000)
                        .correlationKey("JMSCorrelationID"))
                .get();
    }

}
