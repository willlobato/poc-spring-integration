package net.willlobato.poc.spring.integration.main.controller;

import net.willlobato.poc.spring.integration.main.example01.Example01Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.util.concurrent.ListenableFuture;

@MessagingGateway
public interface MessageGateway {

    @Gateway(requestChannel = Example01Configuration.DIRECT_CHANNEL)
    String synchronousCall(String payload);

    @Gateway(requestChannel = Example01Configuration.DIRECT_CHANNEL)
    ListenableFuture<String> doAsync(String payload);

}
