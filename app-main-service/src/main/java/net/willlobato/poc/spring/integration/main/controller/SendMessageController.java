package net.willlobato.poc.spring.integration.main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

    @Autowired
    public MessageGateway messageGateway;

    @GetMapping("/example01_synchronous_call/{message}")
    public String example01SynchronousCall(@PathVariable("message") String message) {
        return messageGateway.synchronousCall(message);
    }

    @GetMapping("/example01_async_call/{message}")
    public void example01AsyncCall(@PathVariable("message") String message) {
        ListenableFuture<String> future = messageGateway.doAsync(message);

        future.addCallback(new ListenableFutureCallback<String>() {
            @Override
            public void onFailure(Throwable ex) {
            }

            @Override
            public void onSuccess(String result) {
                System.out.println(result);
            }
        });
    }

}
