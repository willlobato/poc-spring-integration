package net.willlobato.poc.spring.integration.main.example02;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface QueueGateway {

    @Gateway(requestChannel = PublishQueueConfig.QUEUE_CHANNEL,
            replyChannel = PublishQueueConfig.QUEUE_CHANNEL_REPLY, replyTimeout = 1000, requestTimeout = 100000)
    String send(String payload);

    @Gateway(requestChannel = PublishQueueConfig.QUEUE_CHANNEL)
    void send2(String payload);
}
