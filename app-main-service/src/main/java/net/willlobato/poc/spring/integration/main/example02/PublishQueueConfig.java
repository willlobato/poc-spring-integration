package net.willlobato.poc.spring.integration.main.example02;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableIntegration
public class PublishQueueConfig {

    public static final String QUEUE_2 = "QUEUE2";
    public static final String QUEUE2_REPLY = "QUEUE2_REPLY";

    public static final String QUEUE_CHANNEL = "queue-channel";
    public static final String QUEUE_CHANNEL_REPLY = "queue-channel-reply";

    @Bean(name = QUEUE_CHANNEL)
    public MessageChannel outboundChannel() {
        return new QueueChannel();
    }

    @Bean(name = QUEUE_CHANNEL_REPLY)
    public MessageChannel replyChannel() {
        return new QueueChannel();
    }

    /*
    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata poller() {
        return Pollers.fixedRate(500).get();
    }
    */

    @Bean
    public IntegrationFlow jmsOutboundFlow(ActiveMQConnectionFactory jmsConnectionFactory) {
        return IntegrationFlows
                .from(QUEUE_CHANNEL)
                .handle(Jms.outboundGateway(jmsConnectionFactory)
                        .requestDestination(QUEUE_2)
                        .replyDestination(QUEUE2_REPLY)
                        .receiveTimeout(10000)
                        .correlationKey("JMSCorrelationID"), c -> c.poller(Pollers.fixedRate(1000)))
                .get();
    }

    @Bean
    public IntegrationFlow jmsReplyFlow(ActiveMQConnectionFactory jmsConnectionFactory) {
        return IntegrationFlows.from(Jms.inboundAdapter(jmsConnectionFactory)
                .destination(QUEUE2_REPLY), c -> c.poller(Pollers.fixedRate(1000).maxMessagesPerPoll(10)))
                .channel(QUEUE_CHANNEL_REPLY)
                .get();
    }

}
